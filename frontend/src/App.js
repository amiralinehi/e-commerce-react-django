import React, { useState } from "react";
import { useSelector } from "react-redux";
import Wrapper from "./components/layout/Wrapper";
import Card from "./components/card/Card";
import Sidebar from "./components/sidebar/Sidebar";
import items from "./components/products/Products";
// import LoginModal from "./components/authentication/LoginModal"; // Import the LoginModal component
// import SignupModal from "./components/authentication/SignupModal"; // Import the SignupModal component
import CartModal from "./components/Cart/CartModal";

function App() {
  const [query, setQuery] = useState("all"); // Set a default value of "all" for query
  // const [isLoginModalOpen, setLoginModalOpen] = useState(false); // State for login modal
  // const [isSignupModalOpen, setSignupModalOpen] = useState(false); // State for signup modal
  const [isCartModalOpen, setIsCartModalOpen] = useState(false);

  const cart = useSelector((state) => state.cart.cart);
  console.log("Cart contents:", cart); // Log the contents of the cart

  const onRadioHandler = (event) => {
    event.preventDefault();
    setQuery(event.target.value);
  };

  const OnTagHandler = (event) => {
    event.preventDefault();
    setQuery(event.target.value);
  };

  const OnSearchHandler = (event) => {
    event.preventDefault();
    setQuery(event.target.value);
  };

  const openCartModal = () => {
    setIsCartModalOpen(true);
    // alert("from openCartModal=true");
  };

  const closeCartModal = () => {
    setIsCartModalOpen(false);
    // alert("from openCartModal=false");
  };

  const filteredItems =
    query === "all"
      ? items
      : items.filter(
          (item) =>
            item.category === query ||
            item.description === query ||
            item.name === query
        );

  const result = filteredItems.map((item) => (
    <Card item={item} key={item.id}></Card>
  ));

  // api hooks test
  // console.log(data);

  return (
    <section className="flex">
      {/* <div>
        <button onClick={openLoginModal}>Login</button>
        <button onClick={openSignupModal}>Signup</button>
      </div> */}
      <Sidebar
        className="w-1/4"
        radioHandler={onRadioHandler}
        query={query}
        onModalClickHandler={openCartModal}
      >
        {/* Add buttons to open login and signup modals */}
      </Sidebar>
      <Wrapper
        className="w-full"
        tagHandler={OnTagHandler}
        searchHandler={OnSearchHandler}
        query={query}
      >
        {result}
        {/* Conditionally render login and signup modals */}
        {/* {isLoginModalOpen && <LoginModal onClose={closeLoginModal} />}
        {isSignupModalOpen && <SignupModal onClose={closeSignupModal} />} */}

        {/* Conditionally render cart modal */}
        {isCartModalOpen && <CartModal onClose={closeCartModal} cart={cart} />}
      </Wrapper>
    </section>
  );
}

export default App;
