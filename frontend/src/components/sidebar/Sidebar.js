import { AiOutlineShoppingCart } from "react-icons/ai";

const Sidebar = (props) => {
  return (
    <>
      <div className="bg-white-700 py-4 px-8 flex flex-col justify-stretch items-center bg-slate-50 shadow-inner">
        <div className="mb-8" onClick={props.onModalClickHandler}>
          <AiOutlineShoppingCart className="text-4xl" />
        </div>
        <div className="flex flex-col gap-4 justify-center">
          <div className="flex gap-1">
            <input
              type="radio"
              id="radio0"
              value="all"
              onChange={props.radioHandler}
              name="category"
              checked={props.query === "all"} // Check if query is "all"
            />
            <label htmlFor="radio0">All</label>
          </div>
          <div className="flex gap-1">
            <input
              type="radio"
              id="radio1"
              value="laptop"
              onChange={props.radioHandler}
              name="category"
              checked={props.query === "laptop"} // Check if query is "Laptop"
            />
            <label htmlFor="radio1">Laptop</label>
          </div>
          <div className="flex gap-1">
            <input
              type="radio"
              id="radio2"
              value="camera"
              onChange={props.radioHandler}
              name="category"
              checked={props.query === "camera"} // Check if query is "Camera"
            />
            <label htmlFor="radio2">Camera</label>
          </div>
          <div className="flex gap-1">
            <input
              type="radio"
              id="radio3"
              value="others"
              onChange={props.radioHandler}
              name="category"
              checked={props.query === "others"} // Check if query is "Others"
            />
            <label htmlFor="radio3">Others</label>
          </div>
        </div>
      </div>
    </>
  );
};

export default Sidebar;
