import React from "react";
import { useState } from "react";
import LoginModal from "../authentication/LoginModal";
import SignupModal from "../authentication/SignupModal";

const Navigation = (props) => {
  const [isLoginModalOpen, setLoginModalOpen] = useState(false);
  const [isSignupModalOpen, setSignupModalOpen] = useState(false);

  const openLoginModal = () => {
    setLoginModalOpen(true);
  };

  const closeLoginModal = () => {
    setLoginModalOpen(false);
  };

  const openSignupModal = () => {
    setSignupModalOpen(true);
  };

  const closeSignupModal = () => {
    setSignupModalOpen(false);
  };

  return (
    <>
      <nav className="bg-gray-600 text-white p-4">
        <div className="container mx-auto flex justify-between items-center">
          <a className="text-2xl font-bold" href="/">
            Your Logo
          </a>
          <ul className="space-x-4 flex flex-row">
            <li>
              <a href="/">Home</a>
            </li>
            <li>
              <a href="/about">About</a>
            </li>
            <li>
              <button onClick={openLoginModal}>Login</button>
            </li>
            <li>
              <button onClick={openSignupModal}>Signup</button>
            </li>
            <li>
              <input
                type="search"
                placeholder="search here..."
                onChange={props.searchHandler}
                value={props.query}
                className="text-gray-700"
              ></input>
            </li>
          </ul>
        </div>
      </nav>
      {isLoginModalOpen && <LoginModal onClose={closeLoginModal} />}
      {isSignupModalOpen && <SignupModal onClose={closeSignupModal} />}
    </>
  );
};

export default Navigation;
