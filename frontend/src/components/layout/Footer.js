import React from "react";

const Footer = () => {
  return (
    <footer className="bg-gray-600 text-white p-4 text-center">
      <div className="container mx-auto">
        <p>&copy; 2023 Your Company. All rights reserved.</p>
      </div>
    </footer>
  );
};

export default Footer;
