import React from "react";
import Footer from "./Footer";
import Navigation from "./Navigation";
import Tags from "../tags/Tags";
const Wrapper = (props) => {
  return (
    <div className="flex flex-col min-h-screen w-full">
      <Navigation searchHandler={props.searchHandler} query={props.query} />
      <Tags tagHandler={props.tagHandler}></Tags>
      <div className="flex-grow flex flex-wrap gap-2">{props.children}</div>
      <Footer />
    </div>
  );
};

export default Wrapper;
