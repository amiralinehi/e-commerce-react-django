import Credit from "../credit/Credit";
import { useEffect, useState } from "react";

const CartModal = (props) => {
  const [isCartEmpty, setIsCartEmpty] = useState(null);
  const [totalPrice, setTotalPrice] = useState(0);
  const [isCreditModalOpen, setIsCreditModalOpen] = useState(false);

  const isLoggedIn = localStorage.getItem("isLoggedIn");

  const OpenCreditModal = () => {
    setIsCreditModalOpen(true);
  };

  const CloseCreditModal = () => {
    setIsCreditModalOpen(false);
  };

  useEffect(() => {
    let temp = 0;
    if (props.cart && props.cart.length > 0) {
      props.cart.forEach((element) => {
        temp += parseFloat(element["price"]);
      });
      setTotalPrice(temp);
      setIsCartEmpty(false);
    } else {
      setIsCartEmpty(true);
    }
  }, [props.cart]);

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50 bg-black bg-opacity-50">
      <div className="bg-white p-6 rounded-lg w-80">
        <div className="flex items-center mb-4 gap-4 justify-around bg-blue-500 rounded-md">
          {!isCreditModalOpen && (
            <h2 className="text-2xl font-bold bg-blue-500 rounded-md text-white shadow-sm py-1 px-2">
              Your Cart
            </h2>
          )}
          {!isCreditModalOpen && (
            <button
              className="text-white hover:text-gray-800"
              onClick={props.onClose}
            >
              <svg
                className="h-6 w-6"
                fill="none"
                stroke="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M6 18L18 6M6 6l12 12"
                ></path>
              </svg>
            </button>
          )}
        </div>
        {!isCartEmpty && !isCreditModalOpen && (
          <ul className="bg-blue-50 flex flex-col gap-2 p-1 rounded-sm">
            {props.cart.map((item, index) => (
              <li
                key={item.key}
                className={` flex justify-between items-center shadow-sm mb-2${
                  index < props.cart.length - 1
                    ? " border-b border-blue-200"
                    : ""
                }`}
              >
                <span>{item.name}</span>
                <span>${item.price}</span>
              </li>
            ))}
          </ul>
        )}
        {isCartEmpty && <p>No item to show</p>}

        <div className="flex justify-between items-center mt-2 p-1">
          <span>Total Price: {totalPrice}</span>
          {!isCreditModalOpen && isLoggedIn && (
            <button
              className="text-green-500 px-2 py-1 rounded-lg border border-green-500 shadow-md hover:text-white hover:bg-green-500"
              onClick={OpenCreditModal}
            >
              Buy items
            </button>
          )}
        </div>
        {isCreditModalOpen && <Credit onClose={CloseCreditModal}></Credit>}
      </div>
    </div>
  );
};

export default CartModal;
