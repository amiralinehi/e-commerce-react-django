import laptopImage from "../../media/laptop.avif";
import camerImage from "../../media/camera.jpg";
import trimmerImage from "../../media/trimmer.jpg";

const items = [
  {
    id: 1,
    name: "asus",
    price: "100",
    category: "laptop",
    description: "this is a laptop.",
    image: laptopImage,
  },
  {
    id: 2,
    name: "nikon",
    price: "60",
    category: "camera",
    description: "this is a camera.",
    image: camerImage,
  },
  {
    id: 3,
    name: "trimmer",
    price: "20",
    category: "others",
    description: "trimmer.",
    image: trimmerImage,
  },
];

export default items;
