import { useDispatch } from "react-redux";
import { addToCart, removeFromCart } from "../../store/cartActions";
const Card = (props) => {
  const dispatch = useDispatch();

  // const onIncrementHandler = () => {
  //   setQuantity(quantity + 1);
  // };

  // const onDecrementHandler = () => {
  //   if (quantity > 0) {
  //     setQuantity(quantity - 1);
  //   }
  // };

  const onAddToCart = () => {
    dispatch(addToCart(props.item));
  };

  const onRemoveFromCart = () => {
    dispatch(removeFromCart(props.item));
  };

  return (
    <div className="max-w-xs rounded overflow-hidden shadow-lg m-2 h-min">
      <img src={props.item.image} alt={props.item.name} className="w-64 h-64" />
      <p className="text-gray-600">ID: {props.item.id}</p>
      <div className="px-3 py-2">
        <div className="font-bold text-l mb-2">{props.item.name}</div>
        <p className="text-gray-800 text-base">Price: ${props.item.price}</p>
        <p className="text-gray-800 text-base">
          Category: {props.item.category}
        </p>
        <p className="text-gray-800 text-base">{props.item.description}</p>
        <div className="flex flex-row items-center gap-2">
          <label htmlFor="quantity"></label>
        </div>
        <div className="flex justify-center gap-1 mt-1 py-3 px-6">
          {/* <div className="flex gap-1 shadow-sm">
            <button
              className="bg-white text-blue-800 border border-blue-800 px-2 rounded-md hover:bg-blue-800 hover:text-white"
              onClick={onDecrementHandler}
            >
              -
            </button>
            <button
              className="bg-white text-blue-800 border border-blue-800 px-2 rounded-md hover:bg-blue-800 hover:text-white"
              onClick={onIncrementHandler}
            >
              +
            </button>
          </div> */}
          <button
            className=" text-green-500 px-2 py-1 rounded-lg border border-green-500 shadow-md hover:text-white hover:bg-green-500"
            onClick={onAddToCart}
          >
            Add
          </button>
          <button
            className=" text-blue-300 px-2 py-1 rounded-lg hover:text-blue-800"
            onClick={onRemoveFromCart}
          >
            Remove
          </button>
        </div>
      </div>
    </div>
  );
};

export default Card;
