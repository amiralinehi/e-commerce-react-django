const Tags = (props) => {
  return (
    <section className="bg-slate-200 flex gap-2 p-1">
      <input
        className="bg-white rounded-md py-1 px-2 shadow-md"
        type="button"
        value="laptop"
        onClick={props.tagHandler}
      ></input>
      <input
        className="bg-white rounded-md py-1 px-2 shadow-md"
        type="button"
        value="camera"
        onClick={props.tagHandler}
      ></input>
      <input
        className="bg-white rounded-md py-1 px-2 shadow-md"
        type="button"
        value="others"
        onClick={props.tagHandler}
      ></input>
      <input
        className="bg-white rounded-md py-1 px-2 shadow-md"
        type="button"
        value="all"
        onClick={props.tagHandler}
      ></input>
    </section>
  );
};

export default Tags;
