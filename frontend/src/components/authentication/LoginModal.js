// LoginModal.js
import React, { useRef, useState } from "react";
import useFetch from "../../hooks/useFetch";

function LoginModal({ onClose }) {
  // eslint-disable-next-line no-unused-vars
  const { data, error, fetchData } = useFetch();
  const usernameRef = useRef();
  const PasswordRef = useRef();
  const [loading, setLoading] = useState(false);

  const onLoginHandler = async (event) => {
    event.preventDefault();
    const username = usernameRef.current.value;
    const password = PasswordRef.current.value;

    try {
      setLoading(true);
      const apiUrl = "http://127.0.0.1:8000/api/login/";
      const method = "POST";
      const userInfo = { username: username, password: password };
      const result = await fetchData(apiUrl, method, userInfo);

      // saving result from response in storage
      localStorage.setItem("isLoggedIn", "true");
      localStorage.setItem("token", result["token"]);
      localStorage.setItem("username", result["username"]);
      localStorage.setItem("email", result["email"]);
      localStorage.setItem("phone", result["phone"]);
      localStorage.setItem("address", result["address"]);

      onClose();
      // Handle the response or update the UI based on the data
    } catch (error) {
      console.error(error);
      // Handle error, e.g., show an error message
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50 bg-black bg-opacity-50">
      <div className="bg-white w-96 p-6 rounded-lg shadow-lg">
        {/* Login modal content */}
        <h2 className="text-2xl font-semibold mb-4">Log In</h2>
        <form>
          <div className="mb-4">
            <label
              htmlFor="username"
              className="block text-gray-700 text-sm font-medium mb-1"
            >
              Username
            </label>
            <input
              type="text"
              id="username"
              className="w-full px-3 py-2 border rounded-lg"
              placeholder="Enter your username"
              ref={usernameRef}
            />
          </div>
          <div className="mb-4">
            <label
              htmlFor="password"
              className="block text-gray-700 text-sm font-medium mb-1"
            >
              Password
            </label>
            <input
              type="password"
              id="password"
              className="w-full px-3 py-2 border rounded-lg"
              placeholder="Enter your password"
              ref={PasswordRef}
            />
          </div>
          <button
            className="w-full bg-blue-500 text-white py-2 rounded-lg hover:bg-blue-600"
            type="submit"
            onClick={onLoginHandler}
            disabled={loading} // Disable the button while loading
          >
            {loading ? "Logging In..." : "Log In"}
          </button>
        </form>
        <button
          onClick={onClose}
          className="mt-4 w-full bg-gray-200 text-gray-600 py-2 rounded-lg hover:bg-gray-300"
        >
          Close
        </button>
      </div>
    </div>
  );
}

export default LoginModal;
