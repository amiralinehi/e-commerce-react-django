import React, { useState } from "react";
import useFetch from "../../hooks/useFetch";
import { useRef } from "react";

function SignupModal({ onClose }) {
  const { data, error, fetchData } = useFetch();

  const [loading, setLoading] = useState(false);

  // for signup body
  const usernameRef = useRef();
  const emailRef = useRef();
  const passwordRef = useRef();
  const addressRef = useRef();
  const phoneRef = useRef();
  // sign up handler

  const onSignUpHandler = async (event) => {
    event.preventDefault();
    const body = {
      username: usernameRef.current.value,
      email: emailRef.current.value,
      password: passwordRef.current.value,
      address: addressRef.current.value,
      phone: phoneRef.current.value,
    };

    try {
      setLoading(true);
      const apiUrl = "http://127.0.0.1:8000/api/register/";
      const method = "POST";
      const response = await fetchData(apiUrl, method, body);
      console.log(response);
      onClose();
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50 bg-black bg-opacity-50">
      <div className="bg-white w-96 p-6 rounded-lg shadow-lg">
        {/* Signup modal content */}
        <h2 className="text-2xl font-semibold mb-4">Sign Up</h2>
        <form>
          <div className="mb-4">
            <label
              htmlFor="username"
              className="block text-gray-700 text-sm font-medium mb-1"
            >
              Username
            </label>
            <input
              type="text"
              id="username"
              className="w-full px-3 py-2 border rounded-lg"
              placeholder="Enter your username"
              ref={usernameRef}
            />
          </div>
          <div className="mb-4">
            <label
              htmlFor="email"
              className="block text-gray-700 text-sm font-medium mb-1"
            >
              Email
            </label>
            <input
              type="email"
              id="email"
              className="w-full px-3 py-2 border rounded-lg"
              placeholder="Enter your email"
              ref={emailRef}
            />
          </div>
          <div className="mb-4">
            <label
              htmlFor="password"
              className="block text-gray-700 text-sm font-medium mb-1"
            >
              Password
            </label>
            <input
              type="password"
              id="password"
              className="w-full px-3 py-2 border rounded-lg"
              placeholder="Enter your password"
              ref={passwordRef}
            />
          </div>
          <div className="mb-4">
            <label
              htmlFor="address"
              className="block text-gray-700 text-sm font-medium mb-1"
            >
              Address
            </label>
            <input
              type="text"
              id="address"
              className="w-full px-3 py-2 border rounded-lg"
              placeholder="Enter your address"
              ref={addressRef}
            />
          </div>
          <div className="mb-4">
            <label
              htmlFor="phone"
              className="block text-gray-700 text-sm font-medium mb-1"
            >
              Phone
            </label>
            <input
              type="tel"
              id="phone"
              className="w-full px-3 py-2 border rounded-lg"
              placeholder="Enter your phone number"
              ref={phoneRef}
            />
          </div>
          <button
            className="w-full bg-blue-500 text-white py-2 rounded-lg hover:bg-blue-600"
            type="submit"
            onClick={onSignUpHandler}
            disabled={loading}
          >
            {loading ? "Signing Up..." : "Sign Up"}
          </button>
        </form>
        <button
          onClick={onClose}
          className="mt-4 w-full bg-gray-200 text-gray-600 py-2 rounded-lg hover:bg-gray-300"
        >
          Close
        </button>
      </div>
    </div>
  );
}

export default SignupModal;
