// index.js

import React from "react";
import { createRoot } from "react-dom/client";
import "./index.css";
import { configureStore } from "@reduxjs/toolkit";
import cartReducer from "./store/cartReducer";
import { Provider } from "react-redux";
import App from "./App";
import reportWebVitals from "./reportWebVitals";

const root = createRoot(document.getElementById("root"));

const store = configureStore({
  reducer: {
    cart: cartReducer,
  },
});

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>
);

reportWebVitals();
