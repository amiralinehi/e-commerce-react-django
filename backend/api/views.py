from django.shortcuts import render
from rest_framework import generics
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from .models import customeUser
from .serializer import customeUserSerializer

# Create your views here.

class CustomeUserRegisterView(generics.CreateAPIView):
  queryset = customeUser.objects.all()
  serializer_class = customeUserSerializer

  def create(self, request, *args, **kwargs):
        # Check if the 'password' field is provided in the request data.
        if 'password' not in request.data:
            return Response({'error': 'Password is required for registration.'}, status=status.HTTP_400_BAD_REQUEST)

        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            
            # Check if the 'password' field is empty.
            password = request.data.get('password')
            if not password:
                return Response({'error': 'Password cannot be empty.'}, status=status.HTTP_400_BAD_REQUEST)
            
            user.set_password(password)  # Set the password
            user.save()
            return Response({'message': 'User registered successfully'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ListUsersView(generics.ListAPIView):
  queryset= customeUser.objects.all()
  serializer_class = customeUserSerializer


class CustomObtainAuthToken(ObtainAuthToken):
    def post(self,request,*args,**kwargs):
        serializer = self.serializer_class(data=request.data,context={'request':request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token,created = Token.objects.get_or_create(user=user)

        response_data ={

            'token': token.key,
            'username': user.username,
            'email': user.email,
            'phone': user.phone,
            'address': user.address,
        }

        return Response(response_data)




class LogoutView(generics.GenericAPIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self,request):
        request.auth.delete()
        return Response({'{} logged out successfully'.format(request.user.username)},status=status.HTTP_200_OK)