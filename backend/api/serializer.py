from rest_framework import serializers
from .models import customeUser
from django.contrib.auth import get_user_model

class customeUserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = customeUser
        fields = ('id', 'username', 'email', 'phone', 'address', 'password')

