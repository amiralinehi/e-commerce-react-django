from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token
from api.views import CustomeUserRegisterView,ListUsersView,LogoutView,CustomObtainAuthToken

    # ... Your other URL patterns ...
urlpatterns = [

    # http://127.0.0.1:8000/api/register/
    path('register/', CustomeUserRegisterView.as_view(), name='register-user'),
    path('login/', CustomObtainAuthToken.as_view(), name='login-user'),
    path('logout/', LogoutView.as_view(), name='logout-user'),
    path('users/', ListUsersView.as_view(), name='list-users')
]
